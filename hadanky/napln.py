#!/usr/bin/env python2
import os
import sys
from django.core.wsgi import get_wsgi_application
from hash_pwd import hash_pwd

sys.path=[os.path.realpath('.')]+sys.path
os.environ['DJANGO_SETTINGS_MODULE']='hadanky.settings'
application=get_wsgi_application()

from hadanky.models import *

Hadanka.objects.all().delete()
User.objects.all().delete()
Odpoved.objects.all().delete()
Hodnotenie.objects.all().delete()
Komentar.objects.all().delete()

u1=User(meno='Jozko',priezvisko='Mrkvicka',email='jozko@gmail.com',prezyvka='jojo',heslo=hash_pwd('heslo'))
u2=User(meno='Ferko',priezvisko='Hadanka',email='ferko@gmail.com',prezyvka='feri',heslo=hash_pwd('heslo'))
u3=User(meno='Maria',priezvisko='Bystra',email='bystra@gmail.com',prezyvka='maja',heslo=hash_pwd('heslo'))
u1.save()
u2.save()
u3.save()

h1=Hadanka(nazov='scitavanie',zadanie='spocitaj 3+3',obtiaznost=1,riesenie='6',napoveda='pouzi kalkulacku',autor=u1)
h1.save()
h2=Hadanka(nazov='nasobenie',zadanie='vynasob 3*3',obtiaznost=2,riesenie='9',napoveda='pouzi kalkulacku',autor=u3)
h2.save()

o1=Odpoved(kto=u2,co='5', kcomu=h1)
o1.save()
o2=Odpoved(kto=u2,co='6', kcomu=h1)
o2.save()
o3=Odpoved(kto=u2,co='6', kcomu=h2)
o3.save()
o4=Odpoved(kto=u2,co='9', kcomu=h2)
o4.save()
o5=Odpoved(kto=u1,co='9', kcomu=h2)
o5.save()
'''
///todo to end
for roc in range(1,10):
    for pism in ['A','B','C']:
        t=Trieda(rocnik=roc,pismeno=pism)
        t.save()

f=open('ziaci.dat')
for line in f:
    line=line.strip()
    priezv_meno,roc_pism=line.split(',')
    p,m=priezv_meno.split()
    roc=int(roc_pism[0])
    pism=roc_pism[1:]
    t=Trieda.objects.get(rocnik=roc,pismeno=pism)
    ziak=Ziak(meno=m,priezvisko=p,trieda=t)
    ziak.save()
f.close()
f=open('predmety.dat')
for line in f:
    line=line.strip()
    naz,skr=line.split(';')
    p=Predmet(nazov=naz,skratka=skr)
    p.save()
f.close()
f=open('ucitelia.dat')
for line in f:
    line=line.strip()
    mp,predmety=line.split(';')
    meno,priezvisko=mp.split()
    u=Ucitel(meno=meno,priezvisko=priezvisko)
    u.save()
    for pr_skr in predmety.split(','):
        predmet=Predmet.objects.get(skratka=pr_skr)
        u.predmety.add(predmet)
'''
