# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hadanka',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nazov', models.CharField(max_length=20)),
                ('zadanie', models.CharField(max_length=2000)),
                ('obtiaznost', models.IntegerField()),
                ('riesenie', models.CharField(max_length=20)),
                ('napoveda', models.CharField(max_length=64)),
            ],
            options={
                'verbose_name': 'H\xe1danka',
                'verbose_name_plural': 'H\xe1danky',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hodnotenie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hodnotenie', models.IntegerField()),
                ('hadanka', models.ForeignKey(related_name='hodnotenia', to='hadanky.Hadanka')),
            ],
            options={
                'verbose_name_plural': 'Hodnotenia',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Komentar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=1000)),
                ('kedy', models.DateTimeField(default=django.utils.timezone.now)),
                ('hadanka', models.ForeignKey(related_name='komentare', blank=True, to='hadanky.Hadanka', null=True)),
            ],
            options={
                'verbose_name': 'Koment\xe1r',
                'verbose_name_plural': 'Koment\xe1re',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Odpoved',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('co', models.CharField(max_length=64)),
                ('kedy', models.DateTimeField(default=django.utils.timezone.now)),
                ('kcomu', models.ForeignKey(to='hadanky.Hadanka')),
            ],
            options={
                'verbose_name': 'Odpoved',
                'verbose_name_plural': 'Odpovede',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meno', models.CharField(max_length=64)),
                ('priezvisko', models.CharField(max_length=64)),
                ('email', models.CharField(max_length=64)),
                ('prezyvka', models.CharField(max_length=64)),
                ('heslo', models.CharField(max_length=64)),
            ],
            options={
                'verbose_name': 'U\u017e\xedvate\u013e',
                'verbose_name_plural': 'U\u017e\xedvatelia',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='odpoved',
            name='kto',
            field=models.ForeignKey(to='hadanky.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='komentar',
            name='user',
            field=models.ForeignKey(related_name='mojeKomentare', to='hadanky.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hodnotenie',
            name='user',
            field=models.ForeignKey(to='hadanky.User'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hadanka',
            name='autor',
            field=models.ForeignKey(to='hadanky.User'),
            preserve_default=True,
        ),
    ]
