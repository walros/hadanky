# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from hash_pwd import hash_pwd

class User(models.Model):
    meno        = models.CharField(max_length=64)
    priezvisko  = models.CharField(max_length=64)
    email       = models.CharField(max_length=64)
    prezyvka    = models.CharField(max_length=64)
    heslo       = models.CharField(max_length=64)
    def check_pwd(self,pwd):
        return hash_pwd(pwd)==self.heslo
    def __unicode__(self):
        return u'%s %s' % (self.priezvisko,self.meno)
    class Meta:
        verbose_name=u'Užívateľ'
        verbose_name_plural=u'Užívatelia'

class Hadanka(models.Model):
    nazov       = models.CharField(max_length=20)
    zadanie     = models.CharField(max_length=2000)
    obtiaznost  = models.IntegerField()
    riesenie    = models.CharField(max_length=20)
    napoveda    = models.CharField(max_length=64)
    autor = models.ForeignKey(User,related_name='autorlist')

    def get_riesitelia(self): #TODO odfiltrovat zle odpovede
        return Odpoved.objects.get(kcomu=self).user
    def __unicode__(self):
        return u'%s' % (self.nazov)
    class Meta:
        verbose_name=u'Hádanka'
        verbose_name_plural=u'Hádanky'


class Odpoved(models.Model):
    kto   = models.ForeignKey(User)
    co    = models.CharField(max_length=64)
    kcomu = models.ForeignKey(Hadanka)
    kedy  = models.DateTimeField(default=timezone.now)
    def __unicode__(self):
        return u'%s odpoveda %s na ulohu %s' % (self.kto,self.co,self.kcomu)
    class Meta:
        verbose_name=u'Odpoved'
        verbose_name_plural=u'Odpovede'

class Hodnotenie(models.Model):
    hodnotenie = models.IntegerField()
    hadanka    = models.ForeignKey(Hadanka,related_name="hodnotenia")
    user       = models.ForeignKey(User)
    class Meta:
        verbose_name_plural=u'Hodnotenia'

class Komentar(models.Model):
    text     =models.CharField(max_length=1000)
    user     =models.ForeignKey(User,related_name="mojeKomentare")
    hadanka  =models.ForeignKey(Hadanka,related_name="komentare",null=True,blank=True)
    kedy     =models.DateTimeField(default=timezone.now)
    def __unicode__(self):
        return u'%s' % (self.text)
    class Meta:
        verbose_name=u'Komentár'
        verbose_name_plural=u'Komentáre'
