from django.contrib import admin

import models

admin.site.register(models.User)
admin.site.register(models.Hadanka)
admin.site.register(models.Odpoved)
admin.site.register(models.Hodnotenie)
admin.site.register(models.Komentar)
