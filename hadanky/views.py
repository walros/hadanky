# coding: utf-8
from django.http import HttpResponse,Http404,HttpResponseRedirect
from django.shortcuts import render
from django.template import loader,RequestContext
from django import forms
from django.core.urlresolvers import reverse
from functools import wraps
from models import *

def login_req(view):
    @wraps(view)
    def view_ret(request,*args,**kwargs):
        if "prihlaseny_username" not in request.session:
            return HttpResponseRedirect(reverse("hadanky.views.home"))
        else:
            return view(request,*args,**kwargs)
    return view_ret

class HadankyContext(RequestContext):

    def __init__(self,request,d={}):

        if "prihlaseny_username" in request.session:
            d["hadanky_uzivatel"]=User.objects.get(
                prezyvka=request.session["prihlaseny_username"]
                )
        RequestContext.__init__(self,request,d)

def home(request):
    t=loader.get_template("home.html")
    c=HadankyContext(request)
    html=t.render(c)
    return HttpResponse(html)

class RegistraciaForm(forms.Form):
    username=forms.CharField(max_length=20)
    meno=forms.CharField(max_length=20)
    priezvisko= forms.CharField(max_length=20)
    email=forms.CharField(max_length=20)
    password=forms.CharField(max_length=20,widget=forms.PasswordInput)
    password2=forms.CharField(max_length=20,widget=forms.PasswordInput)

def registracia(request):
    error=None
    if request.method=='POST':
        form=RegistraciaForm(request.POST)
        if form.is_valid():
            meno= form.cleaned_data["meno"]
            priezvisko= form.cleaned_data["priezvisko"]
            email= form.cleaned_data["email"]
            username= form.cleaned_data["username"]
            password = hash_pwd(form.cleaned_data["password"])
            password2 = hash_pwd(form.cleaned_data["password2"])
            if password2 == password :
                if User.objects.filter(prezyvka = username).count() == 0:
                    u=User(meno=meno,priezvisko=priezvisko,email=email,prezyvka=username,heslo=password)
                    u.save()
                    return HttpResponseRedirect(reverse("hadanky.views.login"))
                else:
                    error="zvolena prezyvka je uz obsadena"
            else:
                error="hesla sa nezhoduju"
    else:
        form=RegistraciaForm()
    t=loader.get_template("registracia.html")
    c=HadankyContext(request,{"form":form,"error":error})
    return HttpResponse(t.render(c))

def zoznam_hadaniek(request):
    if "prihlaseny_username" in request.session:
        try:
            u=User.objects.get(prezyvka=request.session["prihlaseny_username"])
            autorlist=u.autorlist.all()
            odpovede= Odpoved.objects.filter(kto=u)
            todolist= Hadanka.objects.all().exclude(autor=u)
            solvelist=[]
            for o in odpovede:
                if o.co == o.kcomu.riesenie :
                    solvelist.append(o.kcomu)
                    todolist=todolist.exclude(nazov=o.kcomu.nazov)

            t=loader.get_template("zoznam_hadaniek.html")
            c=HadankyContext(request,{"autorlist":autorlist,"solvelist":solvelist,"todolist":todolist})
            return HttpResponse(t.render(c))
        except User.DoesNotExist:
            error=u'taky uzivatel neexistuje'
    ulohy=Hadanka.objects.all()
    t=loader.get_template("zoznam_hadaniek.html")
    c=HadankyContext(request,{"hadanky":ulohy})
    return HttpResponse(t.render(c))
def users(request):
    users=User.objects.all()
    t=loader.get_template("users.html")
    c=HadankyContext(request,{"users":users})
    return HttpResponse(t.render(c))

class OdpovedForm(forms.Form):
    odpoved=forms.CharField(max_length=20)
class EditForm(forms.Form):
    zadanie=forms.CharField(widget=forms.Textarea)
    riesenie=forms.CharField(max_length=20)
    napoveda=forms.CharField(max_length=20)
class KomentForm(forms.Form):
    komentar=forms.CharField(widget=forms.Textarea)
@login_req
def hadanka(request,id):
    uloha=Hadanka.objects.get(nazov=id)
    user=User.objects.get(prezyvka=request.session["prihlaseny_username"])
    o = Odpoved.objects.filter(kto=user,kcomu=uloha,co=uloha.riesenie)
    komentare= uloha.komentare.all()
    error=None
    if uloha.autor != user: #riesitel moze zadavat odpoved
        if o.count() >0:#hadanku ma uz vyriesenu
            if request.method=='POST':
                komentForm=KomentForm(request.POST)
                if komentForm.is_valid():
                    kom=Komentar(text=komentForm.cleaned_data["komentar"],hadanka=uloha, user=user)
                    kom.save()
                    t=loader.get_template("hadanka.html")
                    c=HadankyContext(request,{"hadanka":uloha,"komentare":komentare,"komentForm":komentForm})
                    return HttpResponse(t.render(c))
            else:
                komentForm=KomentForm()
            t=loader.get_template("hadanka.html")
            c=HadankyContext(request,{"hadanka":uloha,"komentare":komentare,"komentForm":komentForm})
            return HttpResponse(t.render(c))
        if request.method=='POST':
            form=OdpovedForm(request.POST)
            if form.is_valid():
                odp=Odpoved(kto=user,co=form.cleaned_data["odpoved"],kcomu=uloha)
                odp.save()
                if uloha.riesenie == form.cleaned_data["odpoved"]:
                    error=u'Správna odpoved'
                    t=loader.get_template("hadanka.html")
                    c=HadankyContext(request,{"hadanka":uloha,"komentare":komentare,"komentForm":KomentForm()})
                    return HttpResponse(t.render(c))
                else:
                    error=u'Chybná odpoved'
        else:
            form=OdpovedForm()
    else: #je autor
        if request.method=='POST':
            form=EditForm(request.POST)
            if form.is_valid():
                uloha.zadanie=form.cleaned_data["zadanie"]
                uloha.riesenie=form.cleaned_data["riesenie"]
                uloha.napoveda=form.cleaned_data["napoveda"]
                uloha.save()
                error=u'zmeny boli zaznamenane'
        else:
            form=EditForm(initial={'nazov':uloha.nazov,'zadanie':uloha.zadanie,'riesenie':uloha.riesenie,'napoveda':uloha.napoveda})
    t=loader.get_template("hadanka.html")
    c=HadankyContext(request,{"hadanka":uloha,"form":form,"error":error})
    return HttpResponse(t.render(c))

def user_preview(request,id):
    try:
        u=User.objects.get(prezyvka=id)
        autorlist=u.autorlist.all
        odpovede= Odpoved.objects.filter(kto=u)
        solvelist=[]
        for o in odpovede:
            if o.co == o.kcomu.riesenie :
                solvelist.append(o.kcomu)
        t=loader.get_template("user.html")
        c=HadankyContext(request,{"userinfo":u,"autorlist":autorlist,"solvelist":solvelist})
        return HttpResponse(t.render(c))
    except User.DoesNotExist:
        error=u'taky uzivatel neexistuje'
    t=loader.get_template("user.html")
    c=HadankyContext(request,{"error":error})
    return HttpResponse(t.render(c))

class AddForm(forms.Form):
    nazov=forms.CharField(max_length=20)
    zadanie=forms.CharField(widget=forms.Textarea)
    riesenie=forms.CharField(max_length=20)
    napoveda=forms.CharField(max_length=20)
@login_req
def add(request):
    error=None
    if request.method=='POST':
        form=AddForm(request.POST)
        if form.is_valid():
            nazov= form.cleaned_data["nazov"]
            zadanie= form.cleaned_data["zadanie"]
            riesenie= form.cleaned_data["riesenie"]
            napoveda= form.cleaned_data["napoveda"]
            if Hadanka.objects.filter(nazov = nazov).count() == 0:
                h=Hadanka(autor=User.objects.get(prezyvka=request.session["prihlaseny_username"])
                ,nazov=nazov,zadanie=zadanie,riesenie=riesenie,napoveda=napoveda,obtiaznost=1)
                h.save()
                return HttpResponseRedirect(reverse("hadanky.views.zoznam_hadaniek"))
            else:
                error="hadanka s takym nazvom uz existuje"
    else:
        form=AddForm()
    t=loader.get_template("add.html")
    c=HadankyContext(request,{"form":form,"error":error})
    return HttpResponse(t.render(c))

class LoginForm(forms.Form):
    username=forms.CharField(max_length=20)
    password=forms.CharField(max_length=20,widget=forms.PasswordInput)

def login(request):
    error=None
    if request.method=='POST':
        form=LoginForm(request.POST)
        if form.is_valid():
            try:
                uz=User.objects.get(prezyvka=form.cleaned_data["username"])
                if uz.check_pwd(form.cleaned_data["password"]):
                    request.session["prihlaseny_username"]=uz.prezyvka
                    return HttpResponseRedirect(reverse("hadanky.views.zoznam_hadaniek"))
                else:
                    error=u'Chybné meno alebo heslo'
            except User.DoesNotExist:
                error=u'Chybné meno alebo heslo'
    else:
        form=LoginForm()
    t=loader.get_template("login_form.html")
    c=HadankyContext(request,{"form":form,"error":error})
    return HttpResponse(t.render(c))

def logout(request):

    if "prihlaseny_username" in request.session:
        del request.session["prihlaseny_username"]
    return HttpResponseRedirect(reverse("hadanky.views.home"))
