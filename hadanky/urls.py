from django.conf.urls import patterns, include, url
from django.contrib import admin
import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hadanky.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^home/', views.home),
    url(r'^hadanky$',views.zoznam_hadaniek),
    url(r'^hadanky/(.*)',views.hadanka),
    url(r'^add/',views.add),
    url(r'^user/(.*)$',views.user_preview),
    url(r'^users/',views.users),
    url(r'^login/', views.login),
    url(r'^logout/', views.logout),
    url(r'^registracia/', views.registracia)
)
