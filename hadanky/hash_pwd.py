import hashlib
import settings

def hash_pwd(heslo):
    hasher=hashlib.sha256()
    hasher.update(heslo+settings.SECRET_KEY)
    return hasher.hexdigest()
